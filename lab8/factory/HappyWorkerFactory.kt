package factory

import chain.Handler
import chain.HappyWorkerHandler

class HappyWorkerFactory: AbstractFactory() {
    override fun getHandler(handler: String): Handler {
        var a:Handler?=null
        when(handler){

            "HappyWorkerHandler"->a=HappyWorkerHandler()

        }
        return a!!
    }
}