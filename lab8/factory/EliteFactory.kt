package factory

import chain.CEOHandler
import chain.ExecutiveHandler
import chain.Handler
import chain.ManagerHandler

class EliteFactory: AbstractFactory() {
    override fun getHandler(handler: String): Handler {
        var a:Handler?= null
        when(handler){
            "CEOHandler"->a=CEOHandler()
            "ExecutiveHandler"-> a=ExecutiveHandler()
            "ManagerHandler"-> a=ManagerHandler()
        }

        return a!!
    }
}