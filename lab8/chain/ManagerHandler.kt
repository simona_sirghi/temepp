package chain

class ManagerHandler(var next1: Handler? = null, var next2: Handler? = null): Handler {
    override fun handleRequest(forwardDirection: String, messageToBeProcessed: String) {
        val p=messageToBeProcessed.split(":")

        when(p[0])
        {
            "1"->ExecutiveHandler(next1, next2).handleRequest(forwardDirection, messageToBeProcessed)
            "2"->ExecutiveHandler(next1, next2).handleRequest(forwardDirection, messageToBeProcessed)
            "3"->{
                println("Sunt Manager si prelucrez mesajul "+p[1])
                
            }


            "4"->HappyWorkerHandler(next1, next2).handleRequest(forwardDirection, messageToBeProcessed)
        }
    }
}