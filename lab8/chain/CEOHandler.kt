package chain

import factory.EliteFactory

class CEOHandler(var next1: Handler? = null, var next2: Handler? = null): Handler {
    override fun handleRequest(forwardDirection: String, messageToBeProcessed: String) {
        val p=messageToBeProcessed.split(":")

        when(p[0])
        {
            "1"->print("Sunt CEO si prelucrez mesajul "+p[1])
            "2"->ExecutiveHandler(next1, next2).handleRequest(forwardDirection, messageToBeProcessed)
            "3"->ExecutiveHandler(next1, next2).handleRequest(forwardDirection, messageToBeProcessed)
            "4"->ExecutiveHandler(next1, next2).handleRequest(forwardDirection, messageToBeProcessed)
        }
    }
}