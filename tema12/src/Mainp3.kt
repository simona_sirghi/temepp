import java.util.*
import kotlin.math.sqrt

fun perimetru(x:List<Int>, y:List<Int>, nrLaturi:Int):Double
{
    var per:Double = 0.0
    var aux = x zip y;
    var i = 0

    while(i < nrLaturi - 1)
    {
        val aux1 = Math.abs(aux.get(i+1).first - aux.get(i).first)
        val aux2 = Math.abs(aux.get(i+1).second - aux.get(i).second)
        val sum = aux1 * aux1 + aux2 * aux2
        per += sqrt(sum.toDouble())
        i += 1
    }

    val aux1 = Math.abs(aux.get(0).first - aux.get(nrLaturi - 1).first)
    val aux2 = Math.abs(aux.get(0).second - aux.get(nrLaturi - 1).second)
    val sum = aux1 * aux1 + aux2 * aux2
    per += sqrt(sum.toDouble())

    return per
}

fun main(args: Array<String>) {

    print("Numarul de laturi: ")
    val nrLaturi = readLine()!!
    var i = 0
    print("Introduceti perechile separate prin spatiu; ex: 0 0 , 0 1 , 1 1 , 1 0\n")
    var x = ArrayList<Int>()
    var y = ArrayList<Int>()
    while(i < nrLaturi.toInt())
    {
        i++
        val (a, b) = readLine()!!.split(' ')
        x.add(a.toInt())
        y.add(b.toInt())
    }
    print(perimetru(x, y, nrLaturi.toInt()))


}