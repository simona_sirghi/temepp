import java.util.ArrayDeque

fun List<Int>.eliminare(): List<Int> {
    val filterResults = mutableListOf<Int>()
    this.filterTo(filterResults) { it > 5 }

    return filterResults
}
fun List<Int>.perechi():List<Pair<Int,Int>>{
    var list1 = mutableListOf<Int>()
    var list2 = mutableListOf<Int>()

    for (i in 0..this.size-1)
    {
        if (i % 2 == 0)
        {
            list1.add(this.elementAt(i))
        }
        else
        {
            list2.add(this.elementAt(i))
        }
    }
    return list1.zip(list2)
}
fun List<Pair<Int,Int>>.multiplicare() : List<Int>{
    var aux = mutableListOf<Int>()
    for(i in 0..this.size-1)
    {
        aux.add(this.get(i).first*this.get(i).second)
    }
    return aux
}
fun List<Int>.suma(): List<Int>{
    var aux = mutableListOf<Int>()
    

        aux.add(this.sum())

    return aux
}
fun main(args: Array<String>)
{
    var list = listOf(1, 21, 75,39, 7, 2, 35, 3, 31, 7, 8)

     list = list.eliminare()
    var newlist = list.perechi()

    list = newlist.multiplicare()
    list = list.suma()
    print(list)
}