import java.io.File
import java.io.InputStream
fun caesar(input:String, offset:Int): String {
    var temp = ArrayList<String>()
    var aux = ArrayList<Char>()

    input.split(" ").forEach() {
        aux.clear()
        var o = 0
        if(it.length in 4..7) {
            it.forEach {
                var i = it
                o = 0
                while(o < offset)
                {

                    if(i + 1 > 'z') {
                        i = 'a'
                    }
                    else {
                        i = i + 1
                    }

                    o++
                }
                aux.add(i)
            }
        }

        if(!aux.isEmpty())
            temp.add(aux.joinToString(""))
        else
            temp.add(it)
    }
    return temp.joinToString(" ")
}

fun main(args: Array<String>) {
    var inputStream: InputStream = File("src/input.txt").inputStream()
    var inputString = inputStream.bufferedReader().use { it.readLine() }

    println(caesar(inputString, 5))
}