import os
import struct
from TextASCII import TextASCII
from Binary import Binary
from TextUNICODE import TextUNICODE
from XMLFile import XMLFile
from BMP import BMP


def scanare_adrese(ROOT_DIR):
    ret = []
    for root, subdirs, files, in os.walk(ROOT_DIR):
        for file in os.listdir(root):
            file_path = os.path.join(root, file)
            if os.path.isfile(file_path):
                ret.append(file_path)
    return ret


def verificare_unicode(frecvente):
    total = 0
    for i in range(0, 255):
        total += frecvente[i]

    if frecvente[47] >= 0.3 * total:
        return True
    else:
        return False


def suma(a):
    sum = 0
    for i in range(0, len(a)):
        sum = sum + a[i]
    return sum


def verificare_binary(a, eroare):
    count_true = 0
    count_false = 0
    med = sum(a) / len(a)
    for i in range(0, len(a)):
        if (a[i] <= med + eroare and a[i] >= med - eroare):
            count_true += 1
        else:
            count_false += 1

    if count_true >= count_false:
        return True
    else:
        return False


def verificare_ascii(frecv):
    for i in range(9, 10):
        for j in range(0, 8):
            if (frecv[i] < frecv[j]): return False
        if (frecv[i] < frecv[11] or frecv[i] < frecv[12]): return False
        for j in range(14, 31):
            if (frecv[i] < frecv[j]): return False
        for j in range(128, 255):
            if (frecv[i] < frecv[j]): return False

    for j in range(0, 8):
        if (frecv[13] < frecv[j]): return False
    if (frecv[13] < frecv[11] or frecv[13] < frecv[12]): return False
    for j in range(14, 31):
        if (frecv[13] < frecv[j]): return False
    for j in range(128, 255):
        if (frecv[13] < frecv[j]): return False

    for i in range(32, 127):
        for j in range(0, 8):
            if (frecv[i] < frecv[j]): return False
        if (frecv[i] < frecv[11] or frecv[i] < frecv[12]): return False
        for j in range(14, 31):
            if (frecv[i] < frecv[j]): return False
        for j in range(128, 255):
            if (frecv[i] < frecv[j]): return False

    return True


def calculeaza_frecvente(file_name):
    frecvente = [0] * 256
    f = open(file_name, 'rb')
    try:
        c = f.read()
        for a in c:
            frecvente[a] += 1
    finally:
        f.close()

    return frecvente


def verificare_xml(adresa):
    f = open(adresa, 'rb')
    c = f.read()
    a = c.splitlines()
    if adresa.lower().endswith('.xml'):
        if chr(a[0][0]) == '<' and chr(a[0][len(a[0]) - 1]) == '>':
            return a[0]

    return False


def verificare_bmp(adresa):
    with open(adresa, 'rb') as f:
        tof = f.read(2)
        if (tof != b'BM'):
            return False

    bmp_data = []
    bmp = open("test.bmp", 'rb')
    # type=0
    bmp_data.append(bmp.read(2).decode())
    # size=1
    bmp_data.append(struct.unpack('I', bmp.read(4)))
    # Reserved 1=2
    bmp_data.append(struct.unpack('H', bmp.read(2)))
    # Reserved 2=3
    bmp_data.append(struct.unpack('H', bmp.read(2)))
    # offset=4
    bmp_data.append(struct.unpack('I', bmp.read(4)))
    # dib header size=5
    bmp_data.append(struct.unpack('I', bmp.read(4)))
    # width=6
    bmp_data.append(struct.unpack('I', bmp.read(4)))
    # height=7
    bmp_data.append(struct.unpack('I', bmp.read(4)))
    # color planes=8
    bmp_data.append(struct.unpack('H', bmp.read(2)))
    # bpp=9
    bmp_data.append(struct.unpack('H', bmp.read(2)))

    return bmp_data


def main():
    ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
    adrese = scanare_adrese(ROOT_DIR)

    files = []

    for adresa in adrese:
        frecventa = calculeaza_frecvente(adresa)
        if verificare_ascii(frecventa):
            if verificare_xml(adresa):
                aux = XMLFile()
                aux.frecvente = frecventa
                aux.path_absolut = adresa
                aux.first_tag = verificare_xml(adresa)
            else:
                aux = TextASCII()
                aux.frecvente = frecventa
                aux.path_absolut = adresa
            files.append(aux)

        if verificare_binary(frecventa, 10):
            aux = Binary()
            aux.frecvente = frecventa
            aux.path_absolut = adresa
            files.append(aux)

        if (verificare_bmp(adresa)):
            temp = verificare_bmp(adresa)
            aux = BMP()
            aux.frecvente = frecventa
            aux.path_absolut = adresa
            aux.bpp = temp[9]
            aux.height = temp[7]
            aux.width = temp[6]
            files.append(aux)

        if verificare_unicode(frecventa):
            aux = TextUNICODE()
            aux.frecvente = frecventa
            aux.path_absolut = adresa
            files.append(aux)

    for file in files:
        if type(file).__name__ == "TextASCII":
            print("ASCII", file.path_absolut)
        elif type(file).__name__ == "Binary":
            print("Binary", file.path_absolut)
        elif type(file).__name__ == "TextUNICODE":
            print("Unicode", file.path_absolut)
        elif type(file).__name__ == "XMLfile":
            print("XML", file.path_absolut)
        elif type(file).__name__ == "BMP":
            print("BMP", file.path_absolut)


main()