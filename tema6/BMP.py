from Binary import Binary

class BMP(Binary):
    width = 0
    height = 0
    bpp = 0

    def show_info(self):
        print("Path absolut: " ,self.path_absolut)
        print("Frecvente: ", self.frecvente)
        print("Width: ", self.width)
        print("Height: ", self.height)
        print("BPP: ", self.bpp)