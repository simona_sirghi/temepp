import more_itertools
import datetime
from pprint import pprint
from decimal import Decimal

def mapWord(data):
    return map(lambda x: x[0] + ", " + x, data.split())

def reduceMap(data):
    aux = []
    for d in data:
        aux.append(d.split(', ')[1])
    return more_itertools.map_reduce(aux, lambda word: word[0])

data = 'This sentence has words of various lengths in it, both short ones and long ones'
print(list(mapWord(data)))
lines = list(mapWord(data))

pprint(reduceMap(lines))

