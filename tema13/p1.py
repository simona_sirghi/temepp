from functional import seq
from dataclasses import dataclass
from datetime import timedelta
from datetime import datetime, date, time
import locale
@dataclass
class Person:

    def __init__(self, firtName, lastname, dateOfBirth, emailAdress):
        self.firstName = firtName
        self.lastName = lastname
        self.dateOfBirth = datetime.strptime(dateOfBirth, '%m %d %Y')
        self.emailAdress = emailAdress

    def fullname(self):
        return self.firstName + " " + self.lastName + ", " + str(self.dateOfBirth) + " " + self.emailAdress

if __name__ == '__main__':

    persons = [Person("John", "Doe", '3 11 1996', "jdoe@example.com"), Person("Ellen", "Smith", '5 13 1992', "ellesmith@example.com"), Person("Jane", "White", '2 1 1986', "janewhite@example.com"), Person("Bill", "Jackson", '11 6 1999',"bjackson@example.com"), Person("John", "Smith", '7 14 1975', "johnsmith@example.com"), Person("Jack", "Williams", '5 28 2005', "") ]
    youngest = sorted(seq(persons), key=lambda person: person.dateOfBirth, reverse=True)
    oldest = sorted(seq(persons), key=lambda person: person.dateOfBirth, reverse=False)
    print("youngest person is: ", Person.fullname(youngest[0]))
    print("oldest person is:", Person.fullname(oldest[0]))

    # td = date.today()
    # underage = seq(persons).filter(lambda person: person.dateOfBirth).map(lambda person: int((td-person.dateOfBirth).days /365.25) < 18)
    # print("underage: ", underage)
    email = seq(persons).filter(lambda person: person.emailAdress).map(lambda person: person.emailAdress)
    print("emails:", email)
    peopleToCelebrateEachMonth = seq(persons).group_by(lambda person: person.dateOfBirth.month)
    print("peopleToCelebrateEachMonth:", peopleToCelebrateEachMonth)

