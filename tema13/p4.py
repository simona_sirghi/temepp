lista = range(1,100000)

def run(lista):
    state = 'starea0'
    while(True):
        if state == 'starea0':
            print("Starea 0")
            lista = list(filter(lambda number: all(number%i != 0 for i in range(2, int(number**.5)+1)), lista))
            state = 'starea1'
        elif state == 'starea1':
            print("Starea 1")
            lista = list(filter(lambda number: number % 2 != 0, lista))
            state = 'starea2'
        elif state == 'starea2':
            print("Starea 2")
            lista = list(filter(lambda x: x <= 50, lista))
            state = 'STOP'
        else:
            print(lista)
            break

run(lista)