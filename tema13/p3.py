#1
def maiMicCa5(lis):
    return list(filter(lambda x: x>=5, lis))

#2
def perechi(lis):
    return list(zip(list(li for li in lis if lis.index(li) % 2 == 0), list(li for li in lis if lis.index(li)%2 != 0)))

#3
def multNrPerechi(lis, mult):
    return list(li * mult for li in lis)

#4
def sumList(lis):
    return sum(li for li in lis)
if __name__ == '__main__':
    lis = [1, 21, 75, 39, 7, 2, 35, 3, 7, 8]
    print(maiMicCa5(lis))
    print(perechi(lis))
    print(multNrPerechi(lis, 5))
    print(sumList(lis))