import asyncio
import queue


async def calcul(n):
    await asyncio.sleep(1)
    sum = 0
    for i in range(0, n):
        sum = sum + i
    print(sum)


q = queue.Queue()

q.put(4)
q.put(5)
q.put(7)

asyncio.run(calcul(q.get()))
asyncio.run(calcul(q.get()))
asyncio.run(calcul(q.get()))