public open class AndValue{

    fun and(a: String, b: String):String
    {
         var aux:String=""
        for (i in 0..a.length-1)
        {
            if(a.get(i).equals('1') && b.get(i).equals('1'))
            {
                aux+=a.get(i)
            }
            if(a.get(i).equals('1') && b.get(i).equals('0'))
            {
                aux+=b.get(i)
            }
            if(a.get(i).equals('0') && b.get(i).equals('1'))
            {
                aux+=a.get(i)
            }
            if(a.get(i).equals('0') && b.get(i).equals('0'))
            {
                aux+=a.get(i)
            }

        }
        return aux

    }
}
