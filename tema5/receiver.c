// C Program for Message Queue (Reader Process)
#include <stdio.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <regex.h>


// structure for message queue
struct mesg_buffer {
    long mesg_type;
    char mesg_text[1000];
} message;

int match(const char *string, const char *pattern)
{
    regex_t re;
    if (regcomp(&re, pattern, REG_EXTENDED|REG_NOSUB) != 0) return 0;
    int status = regexec(&re, string, 0, NULL, 0);
    regfree(&re);
    if (status != 0) return 0;
    return 1;
}

int main()
{
    FILE *fp;
    fp = fopen("output.txt", "w+");
  
    // gcc receiver.c -o receiver
    // ./receiver
    key_t key;
    int msgid;

    // ftok to generate unique key
    key = ftok("message_queue_name", 'B');

    // msgget creates a message queue
    // and returns identifier
    msgid = msgget(key, 0666 | IPC_CREAT);

    // msgrcv to receive message
    msgrcv(msgid, &message, sizeof(message), 1, 0);

    // display the message
    printf("Data Received is : %s \n", message.mesg_text);

    //regex

    if(match(message.mesg_text, "<\\s*p[^>]*>(.*?)<\\s*/\\s*p>") && match(message.mesg_text, "<\\s*h1[^>]*>(.*?)<\\s*/\\s*h1>"))
    {
        printf("\nCorect\n");
        fputs(message.mesg_text, fp);

    }
    else
        printf("Gresit");

    fclose(fp);

    // to destroy the message queue
    //msgctl(msgid, IPC_RMID, NULL);

    return 0;
}
