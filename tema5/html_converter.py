import sysv_ipc
import os
import sys
from PyQt5.QtWidgets import QWidget, QApplication, QFileDialog
from PyQt5.uic import loadUi
from PyQt5 import QtCore

def send_message(message_queue, message):
    message_queue.send(message)

def debug_trace(ui=None):
    from pdb import set_trace
    QtCore.pyqtRemoveInputHook()
    set_trace()
    # QtCore.pyqtRestoreInputHook()


class HTMLConverter(QWidget):
    ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
    aux = ""

    def __init__(self):
        super(HTMLConverter, self).__init__()
        ui_path = os.path.join(self.ROOT_DIR, 'html_converter.ui')
        loadUi(ui_path, self)
        self.browse_btn.clicked.connect(self.browse)
        self.c_btn.clicked.connect(self.put_to_queue)
        self.html_btn.clicked.connect(self.to_html)
        self.file_path = None

    # convert text to html
    def to_html(self):
        self.html_txt.setText("")
        rows = []
        
        file = open(self.file_path, "r")
        for row in file:
            rows.append(row)

        rows[0] = "<h1>" + rows[0]
        rows[0] = rows[0].replace("\n", "</h1><br>")

        for i in range(1, len(rows)):
            if rows[i] == "\n":
                rows[i] = rows[i].replace("\n", "<br>")
            else:
                rows[i] = rows[i].replace("\n", "")
                rows[i] = "<p>" + rows[i] + "</p><br>"

        for row in rows:
            self.aux = self.aux + row

        print(self.aux)
        self.html_txt.setText(self.aux)


    #adds a message_input to queue
    def put_to_queue(self):
        print(self.aux)

        message_input = self.aux
        try:
            # put the key (integer) as parameter (in this case: -1)
            message_queue = sysv_ipc.MessageQueue(-1)
            send_message(message_queue, self.aux)
        except sysv_ipc.ExistentialError:
            print("Message queue not initialized. Please run the C program first")

    def browse(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        file, _ = QFileDialog.getOpenFileName(self,
                                              caption='Select file',
                                              directory='',
                                              filter="Text Files (*.txt)",
                                              options=options)
        if file:
            self.file_path = file
            self.path_line_edit.setText(file)
            print(file)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = HTMLConverter()
    window.show()
    sys.exit(app.exec_())
