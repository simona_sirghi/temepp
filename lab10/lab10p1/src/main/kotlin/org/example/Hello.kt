package org.example
import kotlinx.coroutines.*
import java.io.IOException
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.StandardOpenOption
import java.util.concurrent.atomic.AtomicInteger
import kotlin.random.Random
import kotlin.system.*

suspend fun CoroutineScope.massiveRun(action: suspend () -> Unit) {
    val n = 100
    val k = 1000

    val time = measureTimeMillis {
        val jobs = List(n)
        {
            launch { repeat(k) { action()
                var x = AtomicInteger(Random.nextInt(0,1000))
                try {
                    Files.write(Paths.get("src/data.txt"), x.toString().toByteArray(), StandardOpenOption.APPEND)
                } catch (e: IOException) {
                }
            } }
        }
        jobs.forEach { it.join() }
    }
    println("S-au efectuat ${n * k} operatii in $time ms")
}
val mtContext = newFixedThreadPoolContext(2, "mtPool")
var counter = AtomicInteger()
fun main() = runBlocking<Unit> {
    CoroutineScope(mtContext).massiveRun {
        counter.incrementAndGet() //variabila comuna unde vor aparea erori
    }
    println("Numarator = ${counter.get()}")
}
