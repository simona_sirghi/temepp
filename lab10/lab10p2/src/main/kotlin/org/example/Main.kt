package org.example
import kotlinx.coroutines.*
import java.io.IOException
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.StandardOpenOption
import java.util.concurrent.atomic.AtomicInteger
import kotlin.random.Random
import kotlin.system.*
import kotlinx.coroutines.channels.*

sealed class ContorMsg
object IncContor : ContorMsg()
class GetContor(val response: CompletableDeferred<Int>) : ContorMsg()

fun CoroutineScope.counterActor() = actor<ContorMsg> {
    var contor = 0
    for (msg in channel) {
        when (msg) {
            is IncContor -> contor++
            is GetContor -> msg.response.complete(contor)
        }
    }
}

suspend fun CoroutineScope.massiveRun(action: suspend () -> Unit) {
    val n = 100
    val k = 1000
    val time = measureTimeMillis {
        val jobs = List(n)
        {
            launch { repeat(k) { action()
                var x = AtomicInteger(Random.nextInt(0,1000))
                try {
                    Files.write(Paths.get("src/data.txt"), x.toString().toByteArray(), StandardOpenOption.APPEND)
                } catch (e: IOException) {
                }
                    } }
                }
                jobs.forEach { it.join() }
            }
            println("S-au efectuat ${n * k} operatii in $time ms")
        }
fun main() = runBlocking<Unit> {
    val contor = counterActor()
    GlobalScope.massiveRun()
    {
        contor.send(IncContor)
        println(contor.onSend)
    }
    val raspuns = CompletableDeferred<Int>()
    contor.send(GetContor(raspuns))
    println("Contor = ${raspuns.await()}")
    contor.close()

}

