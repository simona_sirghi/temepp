import threading
import multiprocessing
from concurrent.futures import ThreadPoolExecutor
import time
import random

x = list()
for i in range(1000000):
    x.append(random.randint(0, 9))


def check_prime(num):
    if num > 1:
        for i in range(2, num):
            if(num % i) == 0:
                return False
                break
            else:
                return True
    else:
        return False


def countdown1():
    aux = x
    aux.sort()


def countdown2():
    s = {0}
    for i in range(1000000):
        s.add(random.randint(0, 100))
        check_prime(s.pop())


def ver_1():
    thread_1 = threading.Thread(target=countdown2)
    thread_2 = threading.Thread(target=countdown2)
    thread_1.start()
    thread_2.start()
    thread_1.join()
    thread_2.join()


def ver_2():
    countdown2()
    countdown2()


def ver_3():
    process_1 = multiprocessing.Process(target=countdown2)
    process_2 = multiprocessing.Process(target=countdown2)
    process_1.start()
    process_2.start()
    process_1.join()
    process_2.join()


def ver_4():
    with ThreadPoolExecutor(max_workers=2) as executor:
        future = executor.submit(countdown2())
        future = executor.submit(countdown2())


if __name__ == '__main__':
    start = time.time()
    ver_1()
    end = time.time()
    print("\n Timp executie pseudoparalelism cu GIL")
    print(end - start)
    start = time.time()
    ver_2()
    end = time.time()
    print("\n Timp executie secvential")
    print(end - start)
    start = time.time()
    ver_3()
    end = time.time()
    print("\n Timp executie paralela cu multiprocessing")
    print(end - start)
    start = time.time()
    ver_4()
    end = time.time()
    print("\n Timp executie paralela cu concurrent.futures")
    print(end - start)