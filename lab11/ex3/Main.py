
import threading

class Alpha(threading.Thread):
    def __init__(self, list, alpha, pipe, semph):
        super().__init__()
        self.list = list
        self.alpha = alpha
        self.pipe = pipe
        self.semph = semph

    def run(self):
        self.semph.acquire()
        for i in list:
            self.pipe.append(i*self.alpha)
            print(i*self.alpha)
        self.semph.release()

class Sort(threading.Thread):
    def __init__(self, pipe, semph):
        super().__init__()
        self.pipe = pipe
        self.semph = semph

    def run(self):
        self.semph.acquire()
        pipe.sort()
        self.semph.release()
        print("Array: ")
        for i in pipe:
            print(i)

class Print(threading.Thread):
    def __init__(self, pipe, semph):
        super().__init__()
        self.pipe = pipe
        self.semph = semph

    def run(self):
        self.semph.acquire()
        for i in pipe:
            print(i)
        self.semph.release()

if __name__ == '__main__':
    list = []
    pipe = []
    alpha = 3
    for i in range(5):
        list.append(i)
    semph = threading.Semaphore(1)

    a = Alpha(list, alpha, pipe, semph)
    a.start()
    a.join()

    s = Sort(pipe, semph)
    s.start()
    s.join()

    print("sorted array:")
    p = Print(pipe, semph)
    p.start()
    p.join()
