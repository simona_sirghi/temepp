from threading import Thread, Condition
import time



class Consumator(Thread):
    def __init__(self, elemente, conditie):
        Thread.__init__(self)
        self.elemente=elemente
        self.conditie=conditie

    def consumator(self):
        conditie.acquire()
        if len(self.elemente) == 0:
            self.conditie.wait()
            print('mesaj de la consumator: nu am nimic disponibil')
        self.elemente.pop()
        print('mesaj de la consumator : am utlizat un element')
        print('mesaj de la consumator : mai am disponibil', len(self.elemente),
              'elemente')
        self.conditie.notify()
        self.conditie.release()

    def run(self):
        for i in range(5):
            self.consumator()


class Producator(Thread):
    def __init__(self, elemente, conditie):
        Thread.__init__(self)
        self.elemente=elemente
        self.conditie=conditie

    def producator(self):
        self.conditie.acquire()
        if len(self.elemente) == 10:
            self.conditie.wait()
            print('mesaj de la producator : am disponibile', len(self.elemente),
                  'elemente')
            print('mesaj de la producator : am oprit productia')
        self.elemente.append(1)
        print('mesaj de la producator : am produs', len(self.elemente), 'elemente')
        self.conditie.notify()
        self.conditie.release()

    def run(self):
        for i in range(5):
            self.producator()


if __name__ == '__main__':
    elemente = []
    conditie = Condition()
    producator = Producator(elemente, conditie)
    consumator = Consumator(elemente, conditie)
    producator.start()
    consumator.start()
    producator.join()
    consumator.join()