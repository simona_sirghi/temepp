import java.io.File
import java.sql.Date

fun getDataFile(fileName:String):String
{
    var file = File(fileName).readText(Charsets.UTF_8)
    var aux:ArrayList<String> = ArrayList(file.split("\n"))

    aux.removeAt(0)
    aux.removeAt(aux.size - 1)

    return aux.joinToString(separator = "\n")
}

fun dataToArray(fileName:String):ArrayList<String>
{
    var list = getDataFile(fileName).split("\n\n")
    var aux:ArrayList<String> = ArrayList(list)

    return aux
}

fun toHistoryLogRecord(fileName:String): ArrayList<HistoryLogRecord>
{
    var array = arrayListOf<HistoryLogRecord>()
    var list = dataToArray(fileName)
    list.forEach()
    {
        var arr = it.split("\n")
        var time = arr[0].split(": ")[1]

        var command = arr[1].split(": ")[1]

        //year, month, day
        var ymd = time.split("  ")[0].split("-")
        //hour, minute, second
        var hms = time.split("  ")[1].split(":")

        var ts = Timestamp(ymd[0].toInt(), ymd[1].toInt(), ymd[2].toInt(), hms[0].toInt(), hms[1].toInt(), hms[2].toInt())

        var hlr = HistoryLogRecord(ts, command)
        array.add(hlr)
    }

    while(array.size > 50)
        array.removeAt(0)

    return array
}

fun <T : Comparable<T>>max(first: T, second: T): T
{
    val k = first.compareTo(second)
    return if (k >= 0) first else second
}

fun replace(from:HistoryLogRecord, to:HistoryLogRecord, map:HashMap<Timestamp, HistoryLogRecord>)
{
    map.replace(from.ts, from, to)
}

fun main(args: Array<String>) {
    var map = HashMap<Timestamp, HistoryLogRecord>()
    var hlr = toHistoryLogRecord("src/history.txt")

    hlr.forEach()
    {
        map.put(it.ts, it)
    }

    replace(hlr[0],hlr[1],map)

    map.forEach()
    {
        it.key.print()
        print(" -> ${it.value.command}\n")

    }


    var t1 = Timestamp(2020, 10 ,10 ,10 ,10 ,10)
    var t2 = Timestamp(2021, 10 ,10 ,10 ,10 ,10)

    //val a:Timestamp = max(hlr[1].ts,hlr[2].ts)
    val b:HistoryLogRecord = max(hlr[1],hlr[2])
    b.print()
}