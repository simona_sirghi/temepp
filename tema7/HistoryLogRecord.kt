class HistoryLogRecord(t:Timestamp, c:String):Comparable<HistoryLogRecord>
{
    var ts:Timestamp
    var command:String

    init
    {
        ts = t
        command = c
    }

    override fun compareTo(other: HistoryLogRecord):Int
    {
        //if return <0 => ts<other
        //if return =0 => ts=other
        //if return >0 => ts>other

        var aux = 0

        aux = ts.year - other.ts.year
        if(aux !=0)
            return aux

        aux = ts.month - other.ts.month
        if(aux !=0)
            return aux

        aux = ts.day - other.ts.day
        if(aux !=0)
            return aux

        aux = ts.hour - other.ts.hour
        if(aux !=0)
            return aux

        aux = ts.minute - other.ts.minute
        if(aux !=0)
            return aux

        aux = ts.second - other.ts.second
        return aux
    }

    fun print()
    {
        ts.print()
        print(" -> ${command}\n")
    }

}