class Timestamp(var year:Int, var month:Int, var day:Int, var hour:Int, var minute:Int, var second:Int):Comparable<Timestamp>
{
    fun print()
    {
        print("${year}-${month}-${day} ${hour}:${minute}:${second}")
    }

    override fun compareTo(other: Timestamp):Int
    {
        //if return <0 => ts<other
        //if return =0 => ts=other
        //if return >0 => ts>other

        var aux = 0

        aux = this.year - other.year
        if(aux !=0)
            return aux

        aux = this.month - other.month
        if(aux !=0)
            return aux

        aux = this.day - other.day
        if(aux !=0)
            return aux

        aux = this.hour - other.hour
        if(aux !=0)
            return aux

        aux = this.minute - other.minute
        if(aux !=0)
            return aux

        aux = this.second - other.second
        return aux
    }
}