import java.io.File
fun toSyslogRecord(it:String):SyslogRecord {
    var sr_aux = SyslogRecord();
    val arr = it.split(" ").toTypedArray()

    sr_aux.timestamp = arr[0] + " " + arr[1] + " " + arr[2];
    sr_aux.hostname = arr[3];

    if ("[" in arr[4]) {
        var aux = arr[4].split("[");
        sr_aux.application_name = aux[0];
        sr_aux.pid = aux[1].substring(0, aux[1].length - 2);

    }
    else{
            sr_aux.application_name = arr[4].split(":")[0];
        }

    for(i in 5..arr.size-1)
    {
        sr_aux.log_entry+=arr[i]+" ";
    }
    return sr_aux;

}

fun fileToArraySyslogRecord(filename: String):Sequence<SyslogRecord>
{
    var ret = ArrayList<SyslogRecord>()
    File(filename).forEachLine{
        ret.add(toSyslogRecord(it))
    }
    return ret.asSequence()
}

fun toSyslogRecordMap(x:Sequence<SyslogRecord>):Map<String,SyslogRecord>
{
    var map:Map<String,SyslogRecord> = emptyMap()
    x.asIterable().forEach(){
        map = map + mapOf(Pair(it.application_name.toString(),it))

    }
    return map
}


fun main(args: Array<String>)
{
    var x = fileToArraySyslogRecord("src/syslog")
    var map = toSyslogRecordMap(x)

    println(map.get("dnsmasq")?.log_entry)
}