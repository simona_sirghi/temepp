package org.example

import kotlinx.coroutines.*
import java.util.*

suspend fun suma(n:Int):Int
{
    var aux = 0
    var sum = 0

    for(i in 0..n)
    {
        sum = sum + aux
        aux = aux + 1
    }
    return sum
}

fun main()
{
    var vals: Queue<Int> = LinkedList<Int>()
    vals.add(3)
    vals.add(5)
    vals.add(8)
    vals.add(10)


    //corutina 1
    GlobalScope.launch {
        delay(1000)
        println(suma(vals.remove()))
    }

    //corutina 2
    GlobalScope.launch {
        delay(1000)
        println(suma(vals.remove()))
    }

    //corutina 3
    GlobalScope.launch {
        delay(1000)
        println(suma(vals.remove()))
    }

    //corutina 4
    GlobalScope.launch {
        delay(1000)
        println(suma(vals.remove()))
    }

    Thread.sleep(2000) // wait for 2 seconds
    println("Stop")
}

