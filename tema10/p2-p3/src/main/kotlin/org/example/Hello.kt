package org.example
import kotlinx.coroutines.*

suspend fun inmult(v: Array<Int>, alpha: Int): ArrayList<Int>
{
    var elem = 0
    var aux:ArrayList<Int> = ArrayList<Int>()

    for(elem in v.indices)
    {
        aux.add(v[elem] * alpha)
    }

    return aux
}

suspend fun sortare(v: ArrayList<Int>) {
    v.sort()
}

suspend fun afis(ret: ArrayList<Int>) {
    ret.forEach()
    {
        println(it)
    }
}

suspend fun run(v: Array<Int>, alpha: Int)
{
    var ret:ArrayList<Int> = inmult(v,alpha)
    sortare(ret)
    afis(ret)
}

fun main()
{
    var v:Array<Int> = arrayOf(3,1,5,2)
    var alpha = 2

// Start a coroutine
    GlobalScope.launch {
        delay(1000)
        run(v,alpha)
    }
    Thread.sleep(2000) // wait for 2 seconds
    println("Stop")
}

