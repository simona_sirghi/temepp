import factory.FactoryProducer
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

fun main(args: Array<String>) {
    var fp = FactoryProducer();
    var ef = fp.getFactory("EliteFactory");
    var hwf = fp.getFactory("HappyWorkerFactory");

    var ceo1 = ef.getHandler("CEOHandler");
    var ceo2 = ef.getHandler("CEOHandler");

    var executive1 = ef.getHandler("ExecutiveHandler");
    var executive2 = ef.getHandler("ExecutiveHandler");

    var manager1 = ef.getHandler("ManagerHandler");
    var manager2 = ef.getHandler("ManagerHandler");

    var happyworker1 = hwf.getHandler("HappyWorkerHandler");
    var happyworker2 = hwf.getHandler("HappyWorkerHandler");


    GlobalScope.launch {
        delay(1000)
        ceo1.handleRequest("ExecutiveHandler", "3:mes");
    }
    Thread.sleep(2000) // wait for 2 seconds
    println("Stop")


    // se creeaza 1xFactoryProducer, 1xEliteFactory, 1xHappyWorkerFactory

    // crearea instantelor (prin intermediul celor 2 fabrici):
    // 2xCEOHandler, 2xExecutiveHandler, 2xManagerHandler, 2xHappyWorkerHandler
    //...

    // se construieste lantul (se verifica intai diagrama de obiecte si se realizeaza legaturile)
    //...

    // se executa lantul utilizand atat mesaje de prioritate diferita, cat si directii diferite in lant
    //...
}