package factory

class FactoryProducer {
    fun getFactory(choice: String): AbstractFactory {
         var a:AbstractFactory?= null
        when(choice){
            "EliteFactory"->a=EliteFactory()
            "HappyWorkerFactory"->a=HappyWorkerFactory()
        }
        return a!!
    }
}