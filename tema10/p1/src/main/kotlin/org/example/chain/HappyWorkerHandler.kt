package chain

class HappyWorkerHandler(var next1: Handler? = null, var next2: Handler? = null): Handler {
    override suspend fun handleRequest(forwardDirection: String, messageToBeProcessed: String) {
        val p=messageToBeProcessed.split(":")

        when(p[0])
        {
            "1"->ManagerHandler(next1, next2).handleRequest(forwardDirection, messageToBeProcessed)
            "2"->ManagerHandler(next1, next2).handleRequest(forwardDirection, messageToBeProcessed)
            "3"->ManagerHandler(next1, next2).handleRequest(forwardDirection, messageToBeProcessed)
            "4"->println("Sunt HappyWorker si prelucrez mesajul "+p[1])
        }
    }
}