package chain

class ExecutiveHandler(var next1: Handler? = null, var next2: Handler? = null): Handler {
    override suspend fun handleRequest(forwardDirection: String, messageToBeProcessed: String) {
        val p=messageToBeProcessed.split(":")

        when(p[0])
        {
            "1"->CEOHandler(next1, next2).handleRequest(forwardDirection, messageToBeProcessed)
            "2"->println("Sunt Executive si prelucrez mesajul "+p[1])
            "3"->ManagerHandler(next1, next2).handleRequest(forwardDirection, messageToBeProcessed)
            "4"->ManagerHandler(next1, next2).handleRequest(forwardDirection, messageToBeProcessed)
        }
    }
}