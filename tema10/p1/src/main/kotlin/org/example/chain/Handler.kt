package chain

interface Handler {
    suspend fun handleRequest(forwardDirection: String, messageToBeProcessed: String)
}