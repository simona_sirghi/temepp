import itertools


def zippy(*iterable):
    a, b = itertools.tee(iterable)
    next(b, None)
    return list(map(lambda x, y: (x, y), next(a), next(b)))


def zipp(*iterables):
    minlength = float('inf')
    for it in iterables:
        if len(it) < minlength:
            minlength = len(it)
    iterators = [iter(it) for it in iterables]
    count = 0
    while iterators and count < minlength:
        yield tuple(map(next, iterators))
        count += 1


if __name__ == '__main__':
    print(list(zipp([5, 6, 7], [1, 2, 3], [8, 9, 1])))
# print(zippy([5, 6, 7], [1, 2, 3], [8, 9, 1]))


