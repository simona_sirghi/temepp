import math

def is_perfect_square(number:int) -> bool:
    result = False
    sqrt = int(math.sqrt(number))
    result = number == sqrt * sqrt
    return result


def p4(list):
    aux = []
    for l in list:
        if is_perfect_square(l):
            aux.append(l)
    print(aux)

if __name__ == '__main__':
    n = input("n= ")
    list = [x for x in range(int(n))]
    print(list)
    p4(list)
