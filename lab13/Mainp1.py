import types


def decorator_cu_argumente(replacement_fct=None):
    def wrap(f):
        def wrapped_f(*args, **kwargs):
            if replacement_fct and isinstance(replacement_fct, types.FunctionType):
                output = replacement_fct(*args, **kwargs)
            else:
                output = f(*args, **kwargs)
            return output
        return wrapped_f
    return wrap

def replacement(self):
    if self > 1:
        for i in range (2,self):
            if(self % i) == 0:
                return False
            else:
                return True
    else:
        return False

class int(int):
    @decorator_cu_argumente(replacement_fct=replacement)
    def check_prime(self):
        pass



if __name__ == '__main__':
    print("result", int(3).check_prime())

