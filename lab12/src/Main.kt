fun Int.checkPrime(): Boolean{
    var check = false
    for (i in 2..this/2){
        if(this % i ==0){
            check = true
            break
        }
    }
    return !check
}

fun main(args: Array<String>){
    var x: Int = 11
    print(x.checkPrime())
}