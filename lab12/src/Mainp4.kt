import kotlin.properties.Delegates
fun Int.checkP(): Boolean {
    var check = false
    for (i in 2..this / 2) {
        if (this % i == 0) {
            check = true
            break
        }
    }

    return !check;
}
var myIntEven:Int by Delegates.vetoable(0) {
        property, oldValue, newValue ->
    println("${property.name} $oldValue -> $newValue")
    newValue.checkP()
}
fun main() {
    myIntEven = 5
    myIntEven = 2
    println("myIntEven:$myIntEven")
}