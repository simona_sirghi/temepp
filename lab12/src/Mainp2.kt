import java.time.LocalDate
import java.time.format.DateTimeFormatter

fun String.toDate(form:String): LocalDate{
    var formatter = DateTimeFormatter.ofPattern(form)

    return LocalDate.parse(this, formatter)
}

fun main(args: Array<String>){
    var x: String="2020 07 03"
    print(x.toDate("yyyy MM dd"))
}