fun shortString(input:String):String
{
    var seq = input.toCharArray().asSequence()
    var current:Char = seq.first()
    var ret:ArrayList<String> = ArrayList<String>()
    seq.iterator().forEach {
        if(current != it)
        {
            ret.add(current.toString())
            current = it
        }
    }
    ret.add(current.toString())
    return ret.joinToString(separator = "")
}

fun main(args: Array<String>) {
    var str = "aaaabbbccd"

    print(shortString(str))
}