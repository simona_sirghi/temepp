import java.sql.Struct

fun <T, R> Map<T, R>.reversed() = map{(a,b) -> b to a}.toMap()
fun main(args: Array<String>)
{
    var maap = mutableMapOf<Int,String>(1 to "abc", 2 to "def", 3 to "ghi")

    print(maap.reversed())
}