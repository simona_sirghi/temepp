fun compressString(input:String):String
{
    var seq = input.toCharArray().asSequence()
    var count:Int = 0
    var current:Char = seq.first()
    var ret:ArrayList<String> = ArrayList<String>()
    seq.iterator().forEach {
        if(current == it)
        {
            count++}
        else {
            ret.add(current.toString())
            if(count != 1)
                ret.add(count.toString())
            count = 1
            current = it
        }
    }
    ret.add(current.toString())
    if(count != 1)
        ret.add(count.toString())
    return ret.joinToString(separator = "")
}

fun main(args: Array<String>) {
    var str = "aaaabbbccd"

    print(compressString(str))
}