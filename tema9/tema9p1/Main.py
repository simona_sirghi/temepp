import abc
import subprocess
import os
import Command


class Handler(metaclass=abc.ABCMeta):
    def __init__(self):
        self.next_handler = None

    @abc.abstractmethod
    def handle_request(self, file_name):
        pass


class JavaHandler(Handler):
    def handle_request(self, file_name):
        with open(file_name, 'r') as read_obj:
            for line in read_obj:
                if "System.out.print" in line:
                    print("JavaHandler handles")
                    j = Command.JavaC()
                    command_java = Command.JavaCommand(j)
                    j_invoker = Command.Invoker(command_java)
                    j_invoker.invoke()
                    return
        print("JavaHandler doesn't handle. Different type of file")


class BashHandler(Handler):
    def handle_request(self, file_name):
        with open(file_name, 'r') as read_obj:
            for line in read_obj:
                if "#!/bin/bash" in line:
                    print("BashHandler handles ")
                    b = Command.BashC()
                    command_bash = Command.BashCommand(b)
                    b_invoker = Command.Invoker(command_bash)
                    b_invoker.invoke()
                    return
        print("BashHandler doesn't handle ")
        java_handler = JavaHandler()
        java_handler.handle_request(file_name)


class KotlinHandler(Handler):
    def handle_request(self, file_name):
        with open(file_name, 'r') as read_obj:
            for line in read_obj:
                if "fun" in line:
                    print("KotlinHandler handles")
                    k = Command.KotlinC()
                    command_kotlin = Command.KotlinCommand(k)
                    k_invoker = Command.Invoker(command_kotlin)
                    k_invoker.invoke()
                    return
        print("KotlinHandler doesn't handle")
        python_handler = PythonHandler()
        python_handler.handle_request(file_name)


class PythonHandler(Handler):
    def handle_request(self, file_name):
        with open(file_name, 'r') as read_obj:
            for line in read_obj:
                if "__main__" in line:
                    print("PythonHandler handles")
                    p = Command.PythonC()
                    command_python = Command.PythonCommand(p)
                    p_invoker = Command.Invoker(command_python)
                    p_invoker.invoke()
                    return

        print("PythonHandler doesn't handle")
        bash_handler = BashHandler()
        bash_handler.handle_request(file_name)


if __name__ == '__main__':
    chain = KotlinHandler()

    print("First file: ")
    chain.handle_request("test.sh")
    print("Second file: ")
    chain.handle_request("test.py")
    print("Third file: ")
    chain.handle_request("test.java")
    print("Fourth file: ")
    chain.handle_request("test.kt")








