import abc
import subprocess
import os


class Command(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def execute(self):
        pass


class PythonCommand(Command):
    def __init__(self, reciever_python):
        self.reciever_python = reciever_python

    def execute(self):
        self.reciever_python.execute_python()


class JavaCommand(Command):
    def __init__(self, reciever_java):
        self.reciever_java = reciever_java

    def execute(self):
        self.reciever_java.execute_java()


class KotlinCommand(Command):
    def __init__(self, reciever_kotlin):
        self.reciever_kotlin = reciever_kotlin

    def execute(self):
        self.reciever_kotlin.execute_kotlin()


class BashCommand(Command):
    def __init__(self, reciever_bash):
        self.reciever_bash = reciever_bash

    def execute(self):
        self.reciever_bash.execute_bash()


class JavaC:
    def execute_java(self):
        s = subprocess.check_output("javac test.java;java test", shell=True)
        print(s.decode("utf-8"))


class KotlinC:
    def execute_kotlin(self):
        s = subprocess.check_output("kotlinc test.kt -include-runtime -d test.jar;java -jar test.jar", shell=True)
        print(s.decode("utf-8"))


class BashC:
    def execute_bash(self):
        s = subprocess.check_output("chmod +x test.sh;bash test.sh", shell=True)
        print(s.decode("utf-8"))


class PythonC:
    def execute_python(self):
        s = subprocess.check_output("python3 test.py", shell=True)
        print(s.decode("utf-8"))


class Invoker:
    def __init__(self, command):
        self.command = command

    def set_command(self, command):
        self.command = command

    def invoke(self):
        self.command.execute()

