from SelectProductSTM import SelectProductSTM
from TakeMoneySTM import TakeMoneySTM
from Soda import *


class VendingMachineSTM:
    take_money_stm = TakeMoneySTM()
    select_product_stm = SelectProductSTM()

    def proceed_to_checkout(self):
        option = 0

        while option != str(3):
            option = input("1=Introdu bani, 2=Selecteaza produs, 3=Pleaca de la aparat: ")

            if option == str(1):
                self.take_money_stm.add_money()

            if option == str(2):
                self.select_product_stm.choose_another_product()
                if self.select_product_stm.state.price <= self.take_money_stm.state.value:
                    self.take_money_stm.state.value -= self.select_product_stm.state.price
                    print("Ai cumparat: " + self.select_product_stm.state.name + " si mai ai " + str(self.take_money_stm.state.value))
                else:
                    print("Nu ai suficiente fonduri")

