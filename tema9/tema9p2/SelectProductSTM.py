from Soda import *


class SelectProductSTM(object):

    def __init__(self):
        self.state = SelectProduct()

    def change(self, state):
        self.state.switch(state)

    def choose_another_product(self):
        suc = ""
        suc = input("Sucul dorit(cocacola,pepsi,sprite):")
        if suc == "cocacola":
            self.change(CocaCola)

        if suc == "pepsi":
            self.change(Pepsi)

        if suc == "sprite":
            self.change(Sprite)

        print("Acum ai selectat sucul: " + str(self.state.name))
