from State import State


class SelectProduct(State):
    name = "Select Product"
    price = 0


class CocaCola(State):
    name = "Coca Cola"
    price = 2


class Pepsi(State):
    name = "Pepsi"
    price = 3


class Sprite(State):
    name = "Sprite"
    price = 4