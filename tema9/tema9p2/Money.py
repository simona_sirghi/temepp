from MoneyState import MoneyState


class insert_0bani(MoneyState):
    name = "0 bani"
    value = 0


class insert_10bani(MoneyState):
    name = "10 bani"
    value = 0.1


class insert_50bani(MoneyState):
    name = "50 bani"
    value = 0.5


class insert_1leu(MoneyState):
    name = "1 leu"
    value = 1


class insert_5lei(MoneyState):
    name = "5 lei"
    value = 5


class insert_10lei(MoneyState):
    name = "10 lei"
    value = 10