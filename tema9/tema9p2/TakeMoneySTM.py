from Money import *


class TakeMoneySTM(object):
    total_money = 0

    def __init__(self):
        self.state = insert_0bani()

    def change(self, state):
        self.state.switch(state)

    def add_money(self):
        suma = ""
        while suma != "exit":
            suma = input("Suma de adaugat(10bani,50bani,1leu,5lei,10lei,exit):")
            if suma == "10bani":
                self.change(insert_10bani)
                self.total_money += self.state.value

            if suma == "50bani":
                self.change(insert_50bani)
                self.total_money += self.state.value

            if suma == "1leu":
                self.change(insert_1leu)
                self.total_money += self.state.value

            if suma == "5lei":
                self.change(insert_5lei)
                self.total_money += self.state.value

            if suma == "10lei":
                self.change(insert_10lei)
                self.total_money += self.state.value

            print("Acum ai suma de: " + str(self.total_money))



