import time
import requests
import codecs


class Cache:

    # returneaza intr-un dictionar linkurile din cachefile
    def returnFromCache(self):
        f = open("cachefile.txt", "r")
        temp = f.read()
        entries = {}

        request = temp.split("`")

        for i in range(0, len(request)-1):
            aux = request[i].split("~")
            print(aux)
            entries.update({aux[0]: [aux[2], aux[1]]})

        return entries

    def getRequest(self, url):
        entries = self.returnFromCache()
        if url not in entries:
            r = requests.get(url)
            f = open("cachefile.txt", "a")
            f.write(url + "~" + str(round(time.time())) + "~" + r.text + "`")
            print("\nRead from server\nURL: " + url)
            print("Time: " + str(round(time.time())))
            print("Data: " + r.text)
        elif int(entries.get(url, "")[1]) + 3600 < int(round(time.time())):
            r = requests.get(url)
            entries.get(url, "")[0] = str(r.text)
            entries.get(url, "")[1] = str(round(time.time()))

            # Update cache
            ax = ""
            for i in entries:
                ax += i + "~" + str(entries.get(i, "")[1]) + "~" + str(entries.get(i, "")[0]) + "`"

            open("cachefile.txt", "w").write("")
            codecs.open("cachefile.txt", "w", "utf-8").write(ax)
            print("\nUpdate cache\nURL: " + url)
            print("Time: " + entries.get(url, "")[1])
            print("Data: " + entries.get(url, "")[0])
        else:
            print("\nRead from cache\nURL: " + url)
            aux = entries.get(url, "")
            print("Time: " + time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(int(aux[1]))))
            print("Data: " + aux[0])




