from File import File

class JSONFile(File):
    def __init__(self, t, a, p):
        self.title = t
        self.author = a
        self.paragraphs = p

    def read_file_from_stdin(self):
        self.title = input("\nTitle: ")
        self.author = input("\nAuthor: ")
        self.paragraphs = input("\nParagraphs: ")

    def print_json(self):
        print("Title: " + self.title)
        print("Author: " + self.author + "\n")
        for paragraph in self.paragraphs:
            print(paragraph)
