from TextFile import *

class BlogTextFile(File):
    def __init__(self, t, a, p):
        self.template = "Blog"
        self.title = t
        self.author = a
        self.paragraphs = p

    def read_file_from_stdin(self):
        self.title = input("\nTitle: ")
        self.author = input("\nAuthor: ")
        self.paragraphs = input("\nParagraphs: ")

    def print_text(self):
        print("Template: " + self.template)
        print(self.title)
        for paragraph in self.paragraphs:
            print(paragraph)
        print("\nWritten by " + self.author)
