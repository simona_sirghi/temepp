import abc


class File(metaclass=abc.ABCMeta):
    title = ""
    author = ""
    paragraphs = []

    @abc.abstractmethod
    def read_file_from_stdin(self):
        pass

