from TextFile import *

class ArticleTextFile(File):
    def __init__(self, t, a, p):
        self.template = "Article"
        self.title = t
        self.author = a
        self.paragraphs = p

    def read_file_from_stdin(self):
        self.title = input("\nTitle: ")
        self.author = input("\nAuthor: ")
        self.paragraphs = input("\nParagraphs: ")

    def print_text(self):
        print("Template: " + self.template)
        print("\t\t" + self.title)
        print("\t\t\t by " + self.author)
        for paragraph in self.paragraphs:
            print(paragraph)
