from HTMLFile import *
from JSONFile import *
from ArticleTextFile import *
from BlogTextFile import *

class FileFactory:
    @staticmethod
    def FileFactory(choice, t, a, p):
        if choice == "HTML":
            return HTMLFile(t, a, p)
        elif choice == "JSON":
            return JSONFile(t, a, p)
        elif choice == "ArticleTextFile":
            return ArticleTextFile(t, a, p)
        elif choice == "BlogTextFile":
            return BlogTextFile(t, a, p)

