package org.example

import khttp.*
import request.CleanGetRequest
import request.GetRequest
import request.KidsBrowser
import request.PostRequest

fun main(args: Array<String>) {

    //Get request
    var getReq = GetRequest("https://duckduckgo.com/", mapOf("q" to "test"), 1000.0)
    var disallowance = listOf<String>("https://google.com/")
    var cleanReq = CleanGetRequest(getReq, disallowance)

    //Post request
    var postReq = PostRequest("https://duckduckgo.com/", mapOf("q" to "test"))

    var kb:KidsBrowser = KidsBrowser(cleanReq, postReq)

    kb.start()
}

