package request

import khttp.responses.Response
import khttp.delete as httpDelete
import org.json.HTTP

class GetRequest(u:String, p:Map<String, String>, t:Double):HTTPGet
{
    private var timeout:Double = t
    private var genericReq:GenericRequest = GenericRequest(u, p)

    public fun getGenericReq():GenericRequest
    {
        return genericReq
    }

    public fun getTimeout():Double
    {
        return timeout
    }

    public override fun getResponse():Response
    {
        return khttp.get(genericReq.url, genericReq.params, timeout=timeout)
    }
}