package request

import khttp.responses.Response

interface HTTPGet
{
    public fun getResponse(): Response
}