package request

import khttp.responses.GenericResponse
import khttp.responses.Response

class CleanGetRequest(getReq:GetRequest, disallowance:List<String>):HTTPGet
{
    private var parentalControlDisallow:List<String> = disallowance
    private var getRequest:GetRequest = getReq

    override fun getResponse(): Response
    {
        var ok = 1
        parentalControlDisallow.forEach()
        {
            if(it == getRequest.getGenericReq().url)
            {
                ok = 0
            }
        }


        //verifica daca url-ul e pe lista de disallow, si daca e nu returneaza getRequestul pentru a fi afisat in browser
        if(ok == 1)
            return khttp.get(getRequest.getGenericReq().url, getRequest.getGenericReq().params, timeout = getRequest.getTimeout())
        else
            println("Adresa introdusa e pe disallow list, redirectionez catre http://mike.tuiasi.ro/")
            return khttp.get("http://mike.tuiasi.ro/")
    }

}