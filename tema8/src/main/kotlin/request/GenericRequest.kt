package request

open class GenericRequest(u:String, p:Map<String, String>):Cloneable
{
    public var url:String = u
    public var params:Map<String, String> = p

    override fun clone(): Any
    {
        return super.clone()
    }
}