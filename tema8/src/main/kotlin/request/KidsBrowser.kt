package request

class KidsBrowser(cleanReq:CleanGetRequest, postReq:PostRequest)
{
    private var cleanGet:CleanGetRequest = cleanReq
    private var postReq:PostRequest = postReq

    public fun start()
    {
        println("**** GET REQUEST ****")
        println(cleanGet.getResponse().text)

        println("#### POST REQUEST ####")
        println(postReq.postData().text)
    }
}