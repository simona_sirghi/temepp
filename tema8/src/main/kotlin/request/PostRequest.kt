package request

import khttp.post
import khttp.responses.Response
import org.json.HTTP

class PostRequest(u:String, p:Map<String, String>)
{
    private var genericReq:GenericRequest = GenericRequest(u, p)

    public fun postData():Response
    {
        return post(genericReq.url, genericReq.params)
    }
}